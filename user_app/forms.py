from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class SignUpForm(UserCreationForm):
    nama_lengkap = forms.CharField(max_length=50, required=True)
    nomor_id = forms.CharField(max_length=50, required=True)
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    tanggal_lahir = forms.DateField(required=True)
    alamat = forms.CharField(widget=forms.Textarea, required=True)
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

class DokterSignUpForm(SignUpForm):
    id_dokter = forms.CharField(max_length=50, required=True)
    no_sip = forms.CharField(max_length=50, required=True)
    spesialisasi = forms.CharField(widget=forms.Textarea, required=True)

class AdminSignUpForm(SignUpForm):
    nomor_pegawai = forms.CharField(max_length=50, required=True)
    # kode_rs = forms.CharField(max_length=50, required=True)  --TIDAK ADA HRSNYA--

class PasienSignUpForm(SignUpForm):
    no_rekam_medis =  forms.CharField(max_length=50, required=True)
    nama_asuransi =  forms.CharField(max_length=50, required=True)

class LoginForm(forms.Form):
    """Form for user login. Includes username for login and password
    for login. Also created function to test password length"""

    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'Username',
            'name':'username'
        }
    ))

    password = forms.CharField(min_length=8, widget=forms.PasswordInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'Password',
            'name':'password'
        }
    ))
