from django.contrib import admin
from .models import Pengguna, Dokter, Pasien, Admin

admin.site.register(Pengguna)
admin.site.register(Dokter)
admin.site.register(Pasien)
admin.site.register(Admin)

# Register your models here.
