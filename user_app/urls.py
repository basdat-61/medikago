from django.urls import path
from django.conf.urls import url
from .views import landing, signup, login_view
from django.contrib.auth.views import LogoutView
from django.conf import settings

urlpatterns = [
    path('', landing, name='landing'),
    path('masuk', login_view, name='login'),
    path('daftar/', signup, name='daftar'),
    url(r'^keluar/$', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout')
]

