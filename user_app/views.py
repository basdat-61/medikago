from django.shortcuts import render, redirect, reverse
# from django.http import (
#     HttpResponseNotAllowed, HttpResponseRedirect,
#     HttpResponseForbidden, HttpResponseNotFound,
# )
from django.contrib.auth import login as dj_login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .forms import LoginForm, SignUpForm, DokterSignUpForm, PasienSignUpForm, AdminSignUpForm
from django.db import connection

def get_user_role(username, pswd):
    with connection.cursor() as cur:
        cur.execute("select * from dokter where username='{username}'".format(username=username))
        dokter = cur.fetchone()
        if(dokter):
            cur.close()
            return {'role':'Dokter', 'data':dokter}

        cur.execute("select * from administrator where username='{username}'".format(username=username))
        admin = cur.fetchone()
        if(admin):
            cur.close()
            return {'role':'Administrator', 'data':admin}

        cur.execute("select * from pasien where username='{username}'".format(username=username))
        pasien = cur.fetchone()
        if(pasien):
            cur.close()
            return {'role':'Pasien', 'data':pasien}
        
        cur.close()

def landing(request):
    return render(request, 'landing.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            dj_login(request, user)
            return redirect('home')
    else:
        dokter_form = DokterSignUpForm()
        admin_form = AdminSignUpForm()
        pasien_form = PasienSignUpForm()
    return render(request, 'signup.html', {
        'admin_form' : admin_form,
        'dokter_form' : dokter_form,
        'pasien_form' : pasien_form
        })

def login_view(request):

    if(request.method == 'POST'):
        form = LoginForm(request.POST)

        username = request.POST['username']
        password = request.POST['password']
        with connection.cursor() as cur:
            cur.execute("select * from pengguna where username='{username}' and password='{pswd}';".format(username=username, pswd=password))
            user = cur.fetchone()
            print(user)
            cur.close()

        if(user is not None):  
            role = get_user_role(user[1], user[2])
            print(role)
            user = {
                'is_authenticated':True,
                'role': role['role'],
                'email':user[0],
                'username':user[1],
                'nama_lengkap':user[3],
                'nomor_id':user[4],
                'tanggal_lahir':user[5],
                'alamat':user[6],
                'specific_role':role['data']
            }

            request.session['user'] = user
            request.session.set_expiry(7200)

            return redirect("/")
        return redirect('/masuk')

    else:
        form = LoginForm()
    return render(request, 'login.html', {'form':form})

def keluar(request):
    logout(request)
    return redirect(reverse(login_view))
