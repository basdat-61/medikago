from django.db import models
from django.contrib.auth.models import User, AbstractUser

# Create your models here.
class Pengguna(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,  primary_key=True)
    nama_lengkap = models.CharField(max_length=50, blank= False, null=True)
    nomor_id = models.CharField(max_length=50, blank= False, null=True)
    tanggal_lahir = models.DateField(blank= False, null=True)
    alamat = models.TextField(blank= False, null=True)
    is_dokter = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_pasien = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

class Dokter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    id_dokter =  models.CharField(max_length=50, blank= False, primary_key=True)
    no_sip =  models.CharField(max_length=50, blank= False, null=True)
    spesialisasi = models.TextField(blank= False, null=True)

    def __str__(self):
        return self.user.username

class Admin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nomor_pegawai = models.CharField(max_length=50, blank= False, primary_key=True)
    # kode_rs = models.CharField(max_length=50, blank= False, null=True)  --TIDAK ADA HRSNYA--

    def __str__(self):
        return self.user.username

class Pasien(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    no_rekam_medis = models.CharField(max_length=50, blank= False, primary_key=True)
    nama_asuransi = models.CharField(max_length=50, blank= False, null=True)

    def __str__(self):
        return self.user.username
