from django.urls import path, re_path
from .views import medical_supplies_all, buat_obat, buat_resep, update_obat, read_obat, read_resep, delete_obat, delete_resep, update_obat

urlpatterns = [
    path('', medical_supplies_all, name='apotek'),
    path('obat/buat', buat_obat, name='buat_obat'),
    path('resep/buat', buat_resep, name='buat_resep'),
    path('obat/update', update_obat, name='update_obat'),
    path('obat', read_obat, name="read_obat"),
    path('resep', read_resep, name="read_resep"),
    re_path(r'^obat/delete/(?P<kode_obat>[\w-]+)', delete_obat, name="delete_obat"),
    re_path(r'^resep/delete/(?P<no_resep>[\w-]+)', delete_resep, name="delete_resep"),
    re_path(r'^obat/update/(?P<kode_obat>[\w-]+)', update_obat, name="update_obat"),
]

