from django import forms

class ObatRegistrationForm(forms.Form):
    kode = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: PNDL',
            'name':'kode'
        }
    ))

    stok = forms.CharField(widget=forms.NumberInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'1, 2, 3, ...',
            'name':'stok'
        }
    ))

    harga = forms.IntegerField(widget=forms.NumberInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: 90000',
            'name':'harga'
        }
    ))

    komposisi = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: HCl, Morphin',
            'name':'komposisi'
        }
    ), max_length=100, required=False)

    sediaan = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: Tablet',
            'name':'sediaan'
        }
    ))

    merk = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: Panadul',
            'name':'merk'
        }
    ))

class ResepRegistrationForm(forms.Form):

    id_konsultasi = forms.ChoiceField(widget=forms.Select(
        attrs={
            'class':'form-control form-custom my-1',
            'placeholder':'ID Konsultasi'
        }))

    id_transaksi = forms.ChoiceField(widget=forms.Select(
        attrs={
            'class':'form-control form-custom my-1',
            'placeholder':'ID Transaksi'
        }))

    obat = forms.ChoiceField(widget=forms.Select(
        attrs={
            'class':'form-control form-custom my-1',
            'placeholder':'Pilih Obat'
        }), required=False)
    
class ObatUpdateForm(forms.Form):
    kode = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: PNDL',
            'name':'kode'
        }
    ), disabled=True, required=False)

    stok = forms.CharField(widget=forms.NumberInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'1, 2, 3, ...',
            'name':'stok'
        }
    ))

    harga = forms.IntegerField(widget=forms.NumberInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: 90000',
            'name':'harga'
        }
    ))

    komposisi = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: HCl, Morphin',
            'name':'komposisi'
        }
    ), max_length=100, required=False)

    sediaan = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: Tablet',
            'name':'sediaan'
        }
    ))

    merk = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control form-custom',
            'placeholder':'cth: Panadul',
            'name':'merk'
        }
    ))