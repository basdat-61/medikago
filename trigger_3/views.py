from django.shortcuts import render, redirect, render_to_response
from django.views.generic.edit import FormView
from .forms import ObatRegistrationForm, ResepRegistrationForm, ObatUpdateForm
import psycopg2
from django.db import connection

# Create your views here.
def medical_supplies_all(request):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")

    cur.execute("select * from obat;")
    obat_list = cur.fetchall()

    cur.execute("select * from resep;")
    resep_list = cur.fetchall()

    cur.execute("select no_resep, string_agg(kode_obat, ', ') from daftar_obat group by no_resep;")
    daftar_obat = cur.fetchall()
    daftar_obat = list(map(lambda arg: (arg[0], arg[1].split(', ')), daftar_obat))

    cur.close()
    conn.close()
    return render(request, 'apotek.html', {'obat_list':obat_list, 'resep_list':resep_list, 'daftar_obat':daftar_obat})

def buat_obat(request):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")

    if(request.method == 'POST'):
        form = ObatRegistrationForm(request.POST)

        if(form.is_valid()):
            kode = request.POST['kode']
            stok = request.POST['stok']
            harga = request.POST['harga']
            komposisi = request.POST['komposisi']
            sediaan = request.POST['sediaan']
            merk = request.POST['merk']

            query = "insert into medikago.obat (kode, stok, harga, komposisi, bentuk_sediaan, merk_dagang) values ('{kode}', {stok}, {harga}, '{komposisi}', '{sediaan}', '{merk}');".format(kode=kode, stok=stok, harga=harga, komposisi=komposisi, sediaan=sediaan, merk=merk)
            cur.execute(query)
            conn.commit()
            cur.close()
            conn.close()
            return redirect('/resep-obat/obat')
    else:
        form = ObatRegistrationForm()
        cur.close()
        conn.close()
    return render(request, 'obatForm.html', {'form':form})

def buat_resep(request):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")
    
    if(request.method == 'POST'):
        kode_obat = set(request.POST.getlist('obat')).difference({""})
        id_konsultasi = request.POST['id_konsultasi']
        id_transaksi = request.POST['id_transaksi']

        query = "insert into resep (id_konsultasi, no_resep, total_harga, id_transaksi) values ('{id_konsultasi}', 'placeholder', 0, '{id_transaksi}');".format(id_konsultasi=id_konsultasi, id_transaksi=id_transaksi)
        cur.execute(query)
        conn.commit()

        cur.execute("select no_resep from resep order by no_resep desc limit 1;")
        last_no_resep = cur.fetchone()[0]

        for i in kode_obat:
            query = "insert into daftar_obat (no_resep, kode_obat, dosis, aturan) values ('{resep}', '{kode_obat}', '3x sehari sebelum makan', 'Dikonsumsi sesuai petunjuk dokter')".format(resep=last_no_resep, kode_obat=i)
            cur.execute(query)
            conn.commit()

        cur.close()
        conn.close()
        return redirect('/resep-obat/resep')
    else:
        cur.execute("select kode, kode from obat;")
        obat = cur.fetchall()
        obat.insert(0, ("", "Pilih kode obat"))

        cur.execute("select id_transaksi, id_transaksi from transaksi;")
        transaksi = cur.fetchall()

        cur.execute("select id_konsultasi, id_konsultasi from sesi_konsultasi;")
        konsultasi = cur.fetchall()

        cur.close()
        conn.close()

        form = ResepRegistrationForm()
        form.fields['obat'].choices = obat
        form.fields['id_konsultasi'].choices = konsultasi
        form.fields['id_transaksi'].choices = transaksi
    return render(request, 'resepForm.html', {'form':form, 'daftar_obat':obat})

def update_obat(request, kode_obat):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")

    if(request.method == 'POST'):
        form = ObatUpdateForm(data={
            'kode':kode_obat,
            'stok':request.POST['stok'],
            'harga':request.POST['harga'],
            'komposisi':request.POST['komposisi'],
            'sediaan':request.POST['sediaan'],
            'merk' : request.POST['merk']
        })

        if(form.is_valid()):
            stok = request.POST['stok']
            harga = request.POST['harga']
            komposisi = request.POST['komposisi']
            sediaan = request.POST['sediaan']
            merk = request.POST['merk']

            query = "update obat set stok={stok}, harga={harga}, komposisi='{komposisi}', bentuk_sediaan='{sediaan}', merk_dagang='{merk}' where kode='{kode_obat}';".format(stok=stok, harga=harga, komposisi=komposisi, sediaan=sediaan, merk=merk, kode_obat=kode_obat)
            cur.execute(query)
            conn.commit()

            cur.close()
            conn.close()
            return redirect('/resep-obat/obat')
    else:
        query = "select * from obat where kode='{kode_obat}'".format(kode_obat=kode_obat)
        cur.execute(query)
        obat = cur.fetchone()
        form = ObatUpdateForm(initial={'kode':obat[0], 'stok':obat[1], 'harga':obat[2], 'komposisi':obat[3], 'sediaan':obat[4], 'merk':obat[5]})
        cur.close()
        conn.close()
    return render(request, 'obatUpdate.html', {'form':form, 'kode':kode_obat})

def read_obat(request):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")
    cur.execute("select * from obat;")
    obat_list = cur.fetchall();

    cur.close()
    conn.close()

    return render(request, 'obat.html', {'obat_list':obat_list})

def read_resep(request):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")

    cur.execute("select * from resep;")
    resep_list = cur.fetchall()

    cur.execute("select no_resep, string_agg(kode_obat, ', ') from daftar_obat group by no_resep;")
    daftar_obat = cur.fetchall()
    daftar_obat = list(map(lambda arg: (arg[0], arg[1].split(', ')), daftar_obat))

    cur.close()
    conn.close()
    return render(request, 'resep.html', {'resep_list':resep_list, 'daftar_obat':daftar_obat})

def delete_obat(request, kode_obat):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")

    query = "delete from obat where kode='{kode}'".format(kode=kode_obat)
    cur.execute(query)
    conn.commit()

    cur.close()
    conn.close()
    return redirect("/resep-obat/obat")

def delete_resep(request, no_resep):
    conn = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cur = conn.cursor()
    cur.execute("set search_path to medikago;")

    query = "delete from daftar_obat where no_resep='{no_resep}'".format(no_resep=no_resep)
    cur.execute(query)
    conn.commit()
 
    query = "delete from resep where no_resep='{no_resep}'".format(no_resep=no_resep)
    cur.execute(query)
    conn.commit()

    cur.close()
    conn.close()
    return redirect("/resep-obat/resep")