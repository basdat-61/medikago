from django.shortcuts import render, redirect
from .forms import *
from user_app.models import Pengguna
from .query import *
from django.http import JsonResponse

# Create your views here.


def reservasi_sesi_konsultasi(request):
    if request.is_ajax():
        respons = {'id_transaksi': [value for value in retrieve_id_transaksi_given_nrm(request.GET.get('choices'))]}
        return JsonResponse(respons)
    elif request.method == 'POST':
        form = ReservasiSesiKonsultasiForm(request.POST)
        if form.is_valid():
            no_rekam_medis = request.POST['no_rekam_medis']
            tanggal = request.POST['tanggal']
            id_transaksi = request.POST['id_transaksi']

            insert_sesi_konsultasi(no_rekam_medis, tanggal, id_transaksi)

            return redirect('/sesi-konsultasi')
    else:
        form = ReservasiSesiKonsultasiForm()
        konten = {'form' : form,}
        return render(request, 'reservasi_sesi_konsultasi.html', konten)

def sesi_konsultasi(request):
    respon = retrieve_all_sesi_konsultasi()
    if request.user.is_authenticated:
        konten = {'title' : 'Ubah Status Laporan', 'respon' : respon}
        user = request.user
        if user.is_superuser:
            return render(request, 'sesi_konsultasi_admin.html', konten)
        else:
            profile = Pengguna.objects.filter(user=user.id)
            if profile[0].is_admin:
                return render(request, 'sesi_konsultasi_admin.html', konten)
    konten = {'title' : 'Lihat Status Laporan', 'respon' : respon}
    return render(request, 'sesi_konsultasi.html', konten)

def delete_sk(request, delete_id):
    delete_sesi_konsultasi(delete_id)

    return redirect('/sesi-konsultasi')

def update_sk(request, update_id):
    if request.method == 'POST':
        form = UpdateSesiKonsultasiForm(request.POST)
        if form.is_valid():
            new_tanggal = request.POST['tanggal']
            new_status = request.POST['status']

            update_sesi_konsultasi(update_id, new_tanggal, new_status)

            return redirect('/sesi-konsultasi')

    data = retrive_sesi_konsultasi(update_id)
    form = UpdateSesiKonsultasiForm(initial={'tanggal': data['tanggal'], 'status': data['status']})
    konten = {'title' : 'Lihat Status Laporan', 'form' : form, 'value' : data}

    return render(request, 'update_sesi_konsultasi.html', konten)

def registrasi_rs_cabang(request):
    if request.method == 'POST':
        form = RegistrasiRSCabangForm(request.POST)
        if form.is_valid():
            kode_rs = request.POST['kode_rs']
            nama = request.POST['nama']
            tgl = request.POST['tanggal_pendirian']
            jalan = request.POST['jalan']
            nomor = request.POST['nomor']
            kota = request.POST['kota']

            insert_rs_cabang(kode_rs, nama, tgl, jalan, nomor, kota)

            return redirect('/rs-cabang')

    if request.user.is_authenticated:
        form = RegistrasiRSCabangForm()
        konten = {'title' : 'Ubah Status Laporan', 'form' : form}
        user = request.user

        if user.is_superuser:
            return render(request, 'registrasi_rs_cabang.html', konten)
        else:
            profile = Pengguna.objects.filter(user=user.id)
            if profile[0].is_admin:
                return render(request, 'registrasi_rs_cabang.html', konten)
    return redirect('/rs-cabang')

def rs_cabang(request):
    respon = retrieve_all_rs_cabang()
    if request.user.is_authenticated:
        konten = {'title' : 'Ubah Status Laporan', 'respon' : respon}
        user = request.user
        # print(dir(username))
        if user.is_superuser:
            return render(request, 'rs_cabang_admin.html', konten)
        else:
            profile = Pengguna.objects.filter(user=user.id)
            if profile[0].is_admin:
                return render(request, 'rs_cabang_admin.html', konten)
    konten = {'title' : 'Lihat Status Laporan', 'respon' : respon}
    return render(request, 'rs_cabang.html', konten)

def delete_rs(request, delete_id):
    delete_rs_cabang(delete_id)

    return redirect('/rs-cabang')

def update_rs(request, update_id):
    if request.method == 'POST':
        form = UpdateRSCabangForm(request.POST)
        if form.is_valid():
            new_nama = request.POST['nama']
            new_tgl = request.POST['tanggal_pendirian']
            new_jalan = request.POST['jalan']
            new_nomor = request.POST['nomor']
            new_kota = request.POST['kota']

            update_rs_cabang(update_id, new_nama, new_tgl, new_jalan, new_nomor, new_kota)

            return redirect('/rs-cabang')

    data = retrieve_rs_cabang(update_id)
    form = UpdateRSCabangForm(initial={
        'nama': data['nama'],
        'tanggal_pendirian': data['tanggal_pendirian'],
        'jalan': data['jalan'],
        'nomor': data['nomor'],
        'kota': data['kota'],
        })
    konten = {'title' : 'Lihat Status Laporan', 'form' : form, 'value' : data}

    return render(request, 'update_rs_cabang.html', konten)
