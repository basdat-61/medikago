from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from user_app.models import Pengguna, Pasien
from django.db.models.fields import BLANK_CHOICE_DASH
from .query import retrieve_all_no_rekam_medis

DUMMY_TRANSAKSI_CHOICES = []

class ReservasiSesiKonsultasiForm(forms.Form):
    pilihan = [(num, num) for num in retrieve_all_no_rekam_medis()]
    no_rekam_medis = forms.CharField(
        max_length=50, 
        required=True, 
        widget=forms.Select(
            choices=BLANK_CHOICE_DASH + pilihan,
            attrs={'id':'no_rekam_medis_choices'}))
    tanggal = forms.DateField(required=True)
    id_transaksi = forms.CharField(max_length=50, required=True, widget=forms.Select(attrs={'id':'transaksi_choices'}))

class UpdateSesiKonsultasiForm(forms.Form):
    tanggal = forms.CharField(max_length=50, required=True)
    status = forms.CharField(max_length=50, required=True, widget=forms.Select(choices=[('BOOKED', 'BOOKED'), ('MENUNGGU KONFIRMASI', 'MENUNGGU KONFIRMASI'), ('SELESAI', 'SELESAI')]))
    
class RegistrasiRSCabangForm(forms.Form):
    kode_rs = forms.CharField(max_length=50, required=True)
    nama = forms.CharField(max_length=50, required=True)
    tanggal_pendirian = forms.DateField()
    jalan = forms.CharField()
    nomor = forms.IntegerField()
    kota = forms.CharField(required=True)

class UpdateRSCabangForm(forms.Form):
    nama = forms.CharField(max_length=50, required=True)
    tanggal_pendirian = forms.DateField()
    jalan = forms.CharField()
    nomor = forms.IntegerField()
    kota = forms.CharField(required=True)
    