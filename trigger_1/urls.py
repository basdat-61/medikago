from django.urls import path, include
from .views import *
from django.conf.urls import url

app_name = "trigger_1"

urlpatterns = [
    path('sesi-konsultasi/reservasi/', reservasi_sesi_konsultasi, name="reservasi_sesi_konsultasi"),
    path('sesi-konsultasi/', sesi_konsultasi, name="sesi_konsultasi"),
    path('rs-cabang/registrasi/', registrasi_rs_cabang, name="registrasi_rs_cabang"),
    path('rs-cabang/', rs_cabang, name="rs_cabang"),
    url(r'^sesi-konsultasi/delete/(?P<delete_id>[A-Za-z0-9]*)', delete_sk, name="delete_sk"),
    url(r'^sesi-konsultasi/update/(?P<update_id>[A-Za-z0-9]*)', update_sk, name="update_sk"),
    url(r'^rs-cabang/delete/(?P<delete_id>[A-Za-z0-9]*)', delete_rs, name="delete_rs"),
    url(r'^rs-cabang/update/(?P<update_id>[A-Za-z0-9]*)', update_rs, name="update_rs"),
]
