from django.db import connection

def retrieve_all_no_rekam_medis():
    cursor = connection.cursor()

    DB_QUERY = """
    set search_path to medikago;
    select distinct p.no_rekam_medis from pasien as p
    inner join transaksi as t on p.no_rekam_medis = t.no_rekam_medis;
    """
    cursor.execute(DB_QUERY)
    raw_data = cursor.fetchall()
    respond = [data[0] for data in raw_data]

    return respond

def retrieve_id_transaksi_given_nrm(nrm):
    cursor = connection.cursor()

    DB_QUERY = f"""
    select distinct t.id_transaksi from pasien as p
    inner join transaksi as t on p.no_rekam_medis = t.no_rekam_medis
    where t.no_rekam_medis = '{nrm}';
    """

    cursor.execute(DB_QUERY)
    raw_data = cursor.fetchall()
    respond = [data[0] for data in raw_data]

    return respond

def get_last_id_konsultasi():
    cursor = connection.cursor()

    DB_QUERY = f"""
    select id_konsultasi from sesi_konsultasi
    order by 1 DESC;
    """

    cursor.execute(DB_QUERY)
    id = cursor.fetchone()

    return int(id[0][-2:])


def insert_sesi_konsultasi(no_rekam_medis, tanggal, id_transaksi):
    cursor = connection.cursor()
    last_id = 'KONSUL' + str(get_last_id_konsultasi() + 1)
    biaya = 0
    status = 'BOOKED'

    DB_QUERY = f"""
    insert into sesi_konsultasi
    values ('{last_id}', '{no_rekam_medis}', '{tanggal}', '{biaya}', '{status}', '{id_transaksi}');
    """
    cursor.execute(DB_QUERY)
    connection.commit()
    

def retrieve_all_sesi_konsultasi():
    cursor = connection.cursor()

    DB_QUERY = """
    select * from sesi_konsultasi;
    """

    cursor.execute(DB_QUERY)
    raw_data = cursor.fetchall()

    dict_keys = ['id_konsultasi', 'no_rekam_medis_pasien', 'tanggal', 'biaya', 'status', 'id_transaksi']
    respond = []
    for rows in raw_data:
        parsed_data = {key: value for (key, value) in zip(dict_keys, rows)}
        respond.append(dict(parsed_data))

    return respond

def update_sesi_konsultasi(id_konsultasi, tanggal, status):
    cursor = connection.cursor()

    DB_QUERY = f"""
    update sesi_konsultasi
    set tanggal = '{tanggal}', status = '{status}'
    where id_konsultasi = '{id_konsultasi}';
    """
    cursor.execute(DB_QUERY)
    connection.commit()

def delete_sesi_konsultasi(id_konsultasi):
    cursor = connection.cursor()

    DB_QUERY = f"""
    delete from sesi_konsultasi
    where id_konsultasi = '{id_konsultasi}';
    """
    cursor.execute(DB_QUERY)
    connection.commit()

def retrive_sesi_konsultasi(id_konsultasi):
    cursor = connection.cursor()

    DB_QUERY = f"""
    select * from sesi_konsultasi
    where id_konsultasi = '{id_konsultasi}';
    """
    cursor.execute(DB_QUERY)
    raw_data = cursor.fetchone()

    dict_keys = ['id_konsultasi', 'no_rekam_medis_pasien', 'tanggal', 'biaya', 'status', 'id_transaksi']
    parsed_data =  {key: value for (key, value) in zip(dict_keys, raw_data)}

    return dict(parsed_data)

def retrieve_all_rs_cabang():
    cursor = connection.cursor()

    DB_QUERY = """
    select * from rs_cabang;
    """
    cursor.execute(DB_QUERY)
    raw_data = cursor.fetchall()

    dict_keys = ['kode_rs', 'nama', 'tanggal_pendirian', 'jalan', 'nomor', 'kota']
    respond = []
    for rows in raw_data:
        parsed_data = {key: value for (key, value) in zip(dict_keys, rows)}
        respond.append(dict(parsed_data))

    return respond

def retrieve_rs_cabang(kode_rs):
    cursor = connection.cursor()

    DB_QUERY = f"""
    select * from rs_cabang
    where kode_rs = '{kode_rs}';
    """
    cursor.execute(DB_QUERY)
    raw_data = cursor.fetchone()

    dict_keys = ['kode_rs', 'nama', 'tanggal_pendirian', 'jalan', 'nomor', 'kota']
    parsed_data =  {key: value for (key, value) in zip(dict_keys, raw_data)}

    return dict(parsed_data)


def update_rs_cabang(kode_rs, nama, tgl, jalan, nomor, kota):
    cursor = connection.cursor()

    DB_QUERY = f"""
    update rs_cabang
    set nama = '{nama}', tanggal_pendirian = '{tgl}', jalan = '{jalan}', nomor = '{nomor}', kota = '{kota}'
    where kode_rs = '{kode_rs}';
    """

    cursor.execute(DB_QUERY)
    connection.commit()


def delete_rs_cabang(kode_rs):
    cursor = connection.cursor()

    DB_QUERY = f"""
    delete from rs_cabang
    where kode_rs = '{kode_rs}';
    """

    cursor.execute(DB_QUERY)
    connection.commit()
    

def insert_rs_cabang(kode_rs, nama, tgl, jalan, nomor, kota):
    cursor = connection.cursor()

    DB_QUERY = f"""
    insert into rs_cabang
    values ('{kode_rs}', '{nama}', '{tgl}', '{jalan}', '{nomor}', '{kota}');
    """
    cursor.execute(DB_QUERY)
    connection.commit()
    