from django import forms
from rscabang_poli import models

class DaftarkanDokterRSCbgForm(forms.Form):
    id_dokter = forms.ChoiceField(
        label = 'ID Dokter :',
        required = True,
        choices = [('dok001', 'DOK001'),
                   ('dok002', 'DOK002'),
                   ('dok003', 'DOK003'),
                   ('dok004', 'DOK004'),
                   ('dok005', 'DOK005'),
                   ('dok006', 'DOK006'),
                   ('dok007', 'DOK007')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    kode_rs = forms.ChoiceField(
        label = 'Kode RS :',
        required = True,
        choices = [('rs001', 'RS001'),
                   ('rs002', 'RS002'),
                   ('rs003', 'RS003'),
                   ('rs004', 'RS004'),
                   ('rs005', 'RS005')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    # class Meta:
    #     model = models.DaftarkanDokterRSCbg
    #     fields = ('id_dokter', 'kode_rs')

class UpdateDokterRSCbgForm(forms.Form):
    id_dokter = forms.ChoiceField(
        label = 'ID Dokter :',
        required = True,
        choices = [('dok001', 'DOK001'),
                   ('dok002', 'DOK002'),
                   ('dok003', 'DOK003'),
                   ('dok004', 'DOK004'),
                   ('dok005', 'DOK005'),
                   ('dok006', 'DOK006'),
                   ('dok007', 'DOK007')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    kode_rs = forms.ChoiceField(
        label = 'Kode RS :',
        required = True,
        choices = [('rs001', 'RS001'),
                   ('rs002', 'RS002'),
                   ('rs003', 'RS003'),
                   ('rs004', 'RS004'),
                   ('rs005', 'RS005')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    # class Meta:
    #     model = models.UpdateDokterRSCbg
    #     fields = ('id_dokter', 'kode_rs')

class BuatLayananPoliForm(forms.Form):
    nama_layanan = forms.CharField(
        label = 'Nama Layanan',
        required = True,
        max_length = 50,
        widget = forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text'})
    )

    deskripsi = forms.CharField(
        label = 'Deskripsi',
        required = False,
        widget = forms.Textarea(attrs={'class' : 'form-control',
                                       'type' : 'text',
                                       'placeholder' : 'Tulis disini deskripsi layanan poliklinik'})
    )

    kode_rs = forms.ChoiceField(
        label = 'Kode RS :',
        required = True,
        choices = [('rs001', 'RS001'),
                   ('rs002', 'RS002'),
                   ('rs003', 'RS003'),
                   ('rs004', 'RS004'),
                   ('rs005', 'RS005')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    # daftar_jadwal = forms.TimeField(
    #     label = 'Daftar Jadwal',
    #     required = True,
    #     input_formats = ['%H:%M'],
    #     widget = forms.TextInput(attrs={'class' : 'form-control',
    #                                     'placeholder' : '00:00'})
    # )

    class Meta:
        model = models.BuatLayananPoli
        fields = ('nama_layanan', 'deskripsi', 'kode_rs') #'daftar_jadwal'

class UpdateLayananPoliForm(forms.Form):
    #id_poliklinik = forms.Charfield()
    nama_layanan = forms.CharField(
        label = 'Nama Layanan :',
        required = True,
        max_length = 50,
        widget = forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text'})
    )

    deskripsi = forms.CharField(
        label = 'Deskripsi :',
        required = False,
        widget = forms.Textarea(attrs={'class' : 'form-control',
                                       'type' : 'text',
                                       'placeholder' : 'Tulis disini deskripsi layanan poliklinik'})
    )

    kode_rs = forms.ChoiceField(
        label = 'Kode RS :',
        required = True,
        choices = [('rs001', 'RS001'),
                   ('rs002', 'RS002'),
                   ('rs003', 'RS003'),
                   ('rs004', 'RS004'),
                   ('rs005', 'RS005')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    class Meta:
        model = models.BuatLayananPoli
        fields = ('nama_layanan', 'deskripsi', 'kode_rs') #id_poliklinik

class UpdateJadwalPoliForm(forms.Form):
    hari = forms.CharField(
        label = 'Hari :',
        required = True,
        max_length = 50,
        widget = forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text'})
    )

    waktu_mulai = forms.TimeField(
        label = 'Waktu Mulai :',
        required = True,
        input_formats = ['%H:%M'],
        widget = forms.TextInput(attrs={'class' : 'form-control',
                                        'placeholder' : '00:00'})
    )

    waktu_selesai = forms.TimeField(
        label = 'Waktu Selesai :',
        required = True,
        input_formats = ['%H:%M'],
        widget = forms.TextInput(attrs={'class' : 'form-control',
                                        'placeholder' : '00:00'})
    )

    kapasitas = forms.CharField(
        label = 'Kapasitas :',
        required = True,
        max_length = 50,
        widget = forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text'})
    )

    id_dokter = forms.ChoiceField(
        label = 'ID Dokter :',
        required = True,
        choices = [('dok001', 'DOK001'),
                   ('dok002', 'DOK002'),
                   ('dok003', 'DOK003'),
                   ('dok004', 'DOK004'),
                   ('dok005', 'DOK005'),
                   ('dok006', 'DOK006'),
                   ('dok007', 'DOK007')],
        widget = forms.Select(attrs={'class' : 'form-control'})
    )

    #id_poliklinik = forms.Charfield()

    class Meta:
        model = models.BuatLayananPoli
        fields = ('hari', 'waktu_mulai', 'waktu_selesai', 'kapasitas', 'id_dokter') #id_poliklinik