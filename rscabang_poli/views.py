from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from .forms import DaftarkanDokterRSCbgForm, UpdateDokterRSCbgForm, BuatLayananPoliForm, UpdateLayananPoliForm, UpdateJadwalPoliForm
from .models import BuatLayananPoli, UpdateLayananPoli, UpdateJadwalPoli
from datetime import datetime
import psycopg2

def daftarkan_dokter(request):
    connection = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cursor = connection.cursor()
    cursor.execute("SET search_path TO MEDIKAGO;")

    if request.method == 'POST':
        form = DaftarkanDokterRSCbgForm(request.POST)

        if form.is_valid():
            id_dokter = request.POST['id_dokter']
            kode_rs = request.POST['kode_rs']

            query = "INSERT INTO MEDIKAGO.dokter_rs_cabang(id_dokter, kode_rs) VALUES ('{id_dokter}', '{kode_rs}');".format(id_dokter=id_dokter, kode_rs=kode_rs)
            cursor.execute(query)
            connection.commit()
            cursor.close()
            connection.close()

            return redirect ('/list_dokter_rscbg/')
    else:
        form = DaftarkanDokterRSCbgForm()
        cursor.close()
        connection.close()
    
    daftar_dokter_content = {'events_active' : 'active',
                             'form' : form}
    return render(request, 'daftarkan_dokter.html', daftar_dokter_content)

def list_dokter_rscbg(request):
    connection = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cursor = connection.cursor()
    cursor.execute("SET search_path TO MEDIKAGO;")
    cursor.execute("SELECT * FROM dokter_rs_cabang;")
    list_dokter = cursor.fetchall()

    cursor.close()
    connection.close()

    return render(request, 'list_dokter_rscbg.html', {'list_dokter' : list_dokter})

def update_dokter_rscbg(request):
    connection = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cursor = connection.cursor()
    cursor.execute("SET search_path TO MEDIKAGO;")

    if request.method == 'POST':
        form = UpdateDokterRSCbgForm(data={
            'id_dokter' : request.POST['id_dokter'],
            'kode_rs' : request.POST['kode_rs']
        })

        if form.is_valid():
            id_dokter = request.POST['id_dokter']
            kode_rs = request.POST['kode_rs']

            query = "UPDATE dokter_rs_cabang SET id_dokter='{id_dokter}', kode_rs='{kode_rs}';".format(id_dokter=id_dokter, kode_rs=kode_rs)
            cursor.execute(query)
            connection.commit()
            cursor.close()
            connection.close()

            return redirect ('/list_dokter_rscbg/')
    else:
        query = "SELECT * FROM dokter_rs_cabang;"
        cursor.execute(query)
        dokter_rs_cabang = cursor.fetchone()
        form = UpdateDokterRSCbgForm(initial={'id_dokter':dokter_rs_cabang[0], 'kode_rs':dokter_rs_cabang[1]})
        cursor.close()
        connection.close()
    
    update_dokter_content = {'events_active' : 'active',
                             'form' : form}
    return render(request, 'update_dokter_rscbg.html', update_dokter_content)

def buat_layanan_poli(request):
    if request.method == 'POST':
        form = BuatLayananPoliForm(request.POST)
        
        if form.is_valid():
            buat_layanan = BuatLayananPoli(
                nama_layanan = form.data['nama_layanan'],
                deskripsi = form.data['deskripsi'],
                kode_rs = form.data['kode_rs'])
                # daftar_jadwal = form.data['daftar_jadwal'])
            buat_layanan.save()
            return redirect('/list_layanan_poli/')
    else:
        form = BuatLayananPoliForm()

    buat_layanan_content = {'events_active' : 'active',
                            # 'context' : {'now' : datetime.now()},
                            'events' : BuatLayananPoli.objects.all().values(),
                            'form' : form}
    return render(request, 'buat_layanan_poli.html', buat_layanan_content)

def list_layanan_poli(request):
    connection = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cursor = connection.cursor()
    cursor.execute("SET search_path TO MEDIKAGO;")
    cursor.execute("SELECT * FROM layanan_poliklinik;")
    list_layanan_poli = cursor.fetchall()

    cursor.close()
    connection.close()

    return render(request, 'list_layanan_poli.html', {'list_layanan_poli' : list_layanan_poli})

def update_layanan_poli(request):
    if request.method == 'POST':
        form = UpdateLayananPoliForm(request.POST)
        
        if form.is_valid():
            update_layanan = UpdateLayananPoli(
                #id_poliklinik = form.data['id_poliklinik'],
                nama_layanan = form.data['nama_layanan'],
                deskripsi = form.data['deskripsi'],
                kode_rs = form.data['kode_rs'])
            update_layanan.save()
            return redirect('/list_layanan_poli/')
    else:
        form = UpdateLayananPoliForm()

    update_layanan_content = {'events_active' : 'active',
                              'events' : UpdateLayananPoli.objects.all().values(),
                              'form' : form}
    return render(request, 'update_layanan_poli.html', update_layanan_content)

def list_jadwal_poli(request):
    connection = psycopg2.connect(
        user = "lhrayygztbcnsv",
        password = "f26dfeeb8fa3260adca1ec3ad9ae91594eda9f5bec4ac0137fced53c8b1e4638",
        host = "ec2-52-71-55-81.compute-1.amazonaws.com",
        port = "5432",
        database = "dcnb25j03frrad"
    )
    cursor = connection.cursor()
    cursor.execute("SET search_path TO MEDIKAGO;")
    cursor.execute("SELECT * FROM jadwal_layanan_poliklinik;")
    list_jadwal_poli = cursor.fetchall()

    cursor.close()
    connection.close()

    return render(request, 'list_jadwal_poli.html', {'list_jadwal_poli' : list_jadwal_poli})

def update_jadwal_poli(request):
    if request.method == 'POST':
        form = UpdateJadwalPoliForm(request.POST)
        
        if form.is_valid():
            update_jadwal = UpdateJadwalPoli(
                hari = form.data['hari'],
                waktu_mulai = form.data['waktu_mulai'],
                waktu_selesai = form.data['waktu_selesai'],
                kapasitas = form.data['kapasitas'],
                id_dokter = form.data['id_dokter'],)
                #id_poliklinik = form.data['id_poliklinik']
            update_jadwal.save()
            return redirect('/list_layanan_poli/')
    else:
        form = UpdateJadwalPoliForm()

    update_jadwal_content = {'events_active' : 'active',
                              'events' : UpdateJadwalPoli.objects.all().values(),
                              'form' : form}
    return render(request, 'update_jadwal_poli.html', update_jadwal_content)