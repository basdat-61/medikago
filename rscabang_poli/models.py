from django.db import models

# class DaftarkanDokterRSCbg(models.Model):
#     id_dokter = models.CharField(max_length=50)
#     kode_rs = models.CharField(max_length=50)

# class UpdateDokterRSCbg(models.Model):
#     id_dokter = models.CharField(max_length=50)
#     kode_rs = models.CharField(max_length=50)

class BuatLayananPoli(models.Model):
    nama_layanan = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=300)
    kode_rs = models.CharField(max_length=50)
    # daftar_jadwal = models.TimeField()

class UpdateLayananPoli(models.Model):
    #id_poliklinik = forms.Charfield()
    nama_layanan = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=300)
    kode_rs = models.CharField(max_length=50)

class UpdateJadwalPoli(models.Model):
    hari = models.CharField(max_length=50)
    waktu_mulai = models.CharField(max_length=50)
    waktu_selesai = models.CharField(max_length=50)
    kapasitas = models.CharField(max_length=50)
    id_dokter = models.CharField(max_length=50)
    #id_poliklinik = forms.Charfield()