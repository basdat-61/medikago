from django.urls import path
from .views import daftarkan_dokter, list_dokter_rscbg, update_dokter_rscbg, buat_layanan_poli, list_layanan_poli, update_layanan_poli, list_jadwal_poli, update_jadwal_poli

urlpatterns = [
    path('daftarkan_dokter/', daftarkan_dokter, name='daftarkan_dokter'),
    path('list_dokter_rscbg/', list_dokter_rscbg, name = 'list_dokter_rscbg'),
    path('update_dokter_rscbg/', update_dokter_rscbg, name = 'update_dokter_rscbg'),
    path('buat_layanan_poli/', buat_layanan_poli, name='buat_layanan_poli'),
    path('list_layanan_poli/', list_layanan_poli, name='list_layanan_poli'),
    path('update_layanan_poli/', update_layanan_poli, name='update_layanan_poli'),
    path('list_jadwal_poli/', list_jadwal_poli, name='list_jadwal_poli'),
    path('update_jadwal_poli/', update_jadwal_poli, name='update_jadwal_poli'),
]
